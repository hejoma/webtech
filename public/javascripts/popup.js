var popup_aktiv = false;

function zeigePopup() {
	if(popup_aktiv == false) {
        $("#popup-hintergrund").show();
        $("#popup").show();
        popup_aktiv = true;
    }
}

function versteckePopup() {
	if(popup_aktiv == true) {
        $("#popup-hintergrund").hide();
        $("#popup").hide();
        popup_aktiv = false;
    }
}
/*
 * JavaScript für Mensch ärgere dich nicht
 * by Zenti
 *
 */

/* Spielfeld als JSON-Objekt*/
spielfeld = {
	farben : [ "gelb", "blau", "rot", "gruen" ],
	webfarben : {
		gelb : "#f2ff85",
		blau : "#7db4ff",
		rot : "#ff4747",
		gruen : "#5dff87"
	},
	spieler : "",
	farbe : "",
	aktiv : false,

	felder : [ {
		id : "f000",
		x : 233,
		y : 525,
		next : "f001"
	}, {
		id : "f001",
		x : 233,
		y : 473,
		next : "f002"
	}, {
		id : "f002",
		x : 233,
		y : 421,
		next : "f003"
	}, {
		id : "f003",
		x : 233,
		y : 369,
		next : "f004"
	}, {
		id : "f004",
		x : 233,
		y : 317,
		next : "f005"
	}, {
		id : "f005",
		x : 172,
		y : 317,
		next : "f006"
	}, {
		id : "f006",
		x : 122,
		y : 317,
		next : "f007"
	}, {
		id : "f007",
		x : 72,
		y : 317,
		next : "f008"
	}, {
		id : "f008",
		x : 27,
		y : 317,
		next : "f009"
	}, {
		id : "f009",
		x : 27,
		y : 267,
		next : "f010"
	}, {
		id : "f010",
		x : 27,
		y : 213,
		next : "f011"
	}, {
		id : "f011",
		x : 72,
		y : 213,
		next : "f012"
	}, {
		id : "f012",
		x : 122,
		y : 213,
		next : "f013"
	}, {
		id : "f013",
		x : 172,
		y : 213,
		next : "f014"
	}, {
		id : "f014",
		x : 233,
		y : 210,
		next : "f015"
	}, {
		id : "f015",
		x : 233,
		y : 155,
		next : "f016"
	}, {
		id : "f016",
		x : 233,
		y : 103,
		next : "f017"
	}, {
		id : "f017",
		x : 233,
		y : 51,
		next : "f018"
	}, {
		id : "f018",
		x : 233,
		y : 0,
		next : "f019"
	}, {
		id : "f019",
		x : 288,
		y : 0,
		next : "f020"
	}, {
		id : "f020",
		x : 339,
		y : 0,
		next : "f021"
	}, {
		id : "f021",
		x : 339,
		y : 51,
		next : "f022"
	}, {
		id : "f022",
		x : 339,
		y : 100,
		next : "f023"
	}, {
		id : "f023",
		x : 339,
		y : 152,
		next : "f024"
	}, {
		id : "f024",
		x : 339,
		y : 207,
		next : "f025"
	}, {
		id : "f025",
		x : 386,
		y : 207,
		next : "f026"
	}, {
		id : "f026",
		x : 438,
		y : 207,
		next : "f027"
	}, {
		id : "f027",
		x : 492,
		y : 207,
		next : "f028"
	}, {
		id : "f028",
		x : 542,
		y : 207,
		next : "f029"
	}, {
		id : "f029",
		x : 542,
		y : 267,
		next : "f030"
	}, {
		id : "f030",
		x : 542,
		y : 317,
		next : "f031"
	}, {
		id : "f031",
		x : 492,
		y : 317,
		next : "f032"
	}, {
		id : "f032",
		x : 438,
		y : 317,
		next : "f033"
	}, {
		id : "f033",
		x : 386,
		y : 317,
		next : "f034"
	}, {
		id : "f034",
		x : 339,
		y : 317,
		next : "f035"
	}, {
		id : "f035",
		x : 339,
		y : 369,
		next : "f036"
	}, {
		id : "f036",
		x : 339,
		y : 418,
		next : "f037"
	}, {
		id : "f037",
		x : 339,
		y : 470,
		next : "f038"
	}, {
		id : "f038",
		x : 339,
		y : 525,
		next : "f039"
	}, {
		id : "f039",
		x : 288,
		y : 525,
		next : "f000"
	}

	],

	homes : {
		gelb : {
			start : [ {
				id : "f040",
				x : 25,
				y : 470
			}, {
				id : "f041",
				x : 78,
				y : 470
			}, {
				id : "f042",
				x : 25,
				y : 528
			}, {
				id : "f043",
				x : 78,
				y : 528
			} ],
			startfeld : "f000",
			endfeld : "f039"

		},

		blau : {
			start : [ {
				id : "f044",
				x : 488,
				y : 0
			}, {
				id : "f045",
				x : 543,
				y : 0
			}, {
				id : "f046",
				x : 488,
				y : 50
			}, {
				id : "f047",
				x : 543,
				y : 50
			} ],
			startfeld : "f020",
			endfeld : "f019"
		},

		rot : {
			start : [ {
				id : "f048",
				x : 25,
				y : 0
			}, {
				id : "f049",
				x : 78,
				y : 0
			}, {
				id : "f050",
				x : 25,
				y : 50
			}, {
				id : "f051",
				x : 78,
				y : 50
			} ],
			startfeld : "f010",
			endfeld : "f009"
		},

		gruen : {
			start : [ {
				id : "f052",
				x : 490,
				y : 470
			}, {
				id : "f053",
				x : 543,
				y : 470
			}, {
				id : "f054",
				x : 490,
				y : 528
			}, {
				id : "f055",
				x : 543,
				y : 528
			} ],
			startfeld : "f030",
			endfeld : "f029"
		}
	},

	finish : {
		gelb : [ {
			id : "f056",
			x : 286,
			y : 317
		}, {
			id : "f057",
			x : 286,
			y : 369
		}, {
			id : "f058",
			x : 286,
			y : 418
		}, {
			id : "f059",
			x : 286,
			y : 470
		} ],

		blau : [ {
			id : "f060",
			x : 286,
			y : 207
		}, {
			id : "f061",
			x : 286,
			y : 152
		}, {
			id : "f062",
			x : 286,
			y : 100
		}, {
			id : "f063",
			x : 286,
			y : 51
		} ],

		rot : [ {
			id : "f064",
			x : 230,
			y : 265
		}, {
			id : "f065",
			x : 180,
			y : 265
		}, {
			id : "f066",
			x : 127,
			y : 265
		}, {
			id : "f067",
			x : 74,
			y : 265
		} ],

		gruen : [ {
			id : "f068",
			x : 339,
			y : 265
		}, {
			id : "f069",
			x : 392,
			y : 265
		}, {
			id : "f070",
			x : 444,
			y : 265
		}, {
			id : "f071",
			x : 495,
			y : 265
		}

		]

	},

	figuren : {
		gelb : [ "f040", "f041", "f042", "f043" ],
		blau : [ "f045", "f046", "f047", "f048" ],
		rot : [ "f049", "f050", "f051", "f052" ],
		gruen : [ "f053", "f054", "f055", "f056" ]
	},

	wuerfel : {
		gelb : {
			x : 150,
			y : 500
		},
		rot : {
			x : 150,
			y : 40
		},
		blau : {
			x : 400,
			y : 40
		},
		gruen : {
			x : 400,
			y : 500
		},
		besitzer : ""
	}

};

function setze(str, x, y) {
	$(str).css('margin-left', x);
	$(str).css('margin-top', y);
}

function verstecke(farbe) {
	for ( var i = 0; i < 4; i++) {
		$("#" + farbe + (i + 1)).hide();
	}
}

function zeige(farbe) {
	for ( var i = 0; i < 4; i++) {
		$("#" + farbe + (i + 1)).show();
	}
}

function select(farbe,nummer) {
	if (spielfeld.aktiv == true && spielfeld.farbe == farbe) {
		webSocket.send("Figur:" + nummer);
		
	}

}

function setzeFeld(farbe, nummer, feldid) {
	var str = "#" + farbe + (nummer + 1);
	for ( var i = 0; i < spielfeld.felder.length; i++) {
		if (spielfeld.felder[i].id == feldid) {
			var x = spielfeld.felder[i].x;
			var y = spielfeld.felder[i].y;
			setze(str, x, y);
			spielfeld.figuren[farbe][nummer] = feldid;
		}
	}
}

/* Figur aufs Startfeld setzen */
function setzeStart(farbe, nummer) {

	setzeFeld(farbe, nummer, spielfeld.homes[farbe].startfeld);

}

/* Figur in den Home-Bereich stellen */
function setzeHome(farbe, nummer) {
	var str = "#" + farbe + (nummer + 1);
	setze(str, spielfeld.homes[farbe].start[nummer].x,
			spielfeld.homes[farbe].start[nummer].y);
	spielfeld.figuren[farbe][nummer] = spielfeld.homes[farbe].start[nummer].id;
}

/* Figur auf Zielposition stellen */
function setzeZiel(farbe, nummer) {
	var str = "#" + farbe + (nummer + 1);
	setze(str, spielfeld.finish[farbe][nummer].x,
			spielfeld.finish[farbe][nummer].y);
	spielfeld.figuren[farbe][nummer] = spielfeld.finish[farbe][nummer].id;
}

function naechstesFeld(farbe, nummer) {
	/* Überprüfen, ob Figur auf Spielfeld steht */
	if (spielfeld.figuren[farbe][nummer] == spielfeld.finish[farbe][nummer].id) {
		return -1;
	}
	if (spielfeld.figuren[farbe][nummer] == spielfeld.homes[farbe].start[nummer].id) {
		return -1;
	}
	/* Überprüfen, ob Figur schon eine Runde vollendet hat */
	if (spielfeld.figuren[farbe][nummer] == spielfeld.homes[farbe].endfeld) {
		setzeZiel(farbe, nummer);

	} else {
		for ( var i = 0; i < spielfeld.felder.length; i++) {
			if (spielfeld.figuren[farbe][nummer] == spielfeld.felder[i].id) {
				setzeFeld(farbe, nummer, spielfeld.felder[i].next);
				break;
			}
		}
	}
	return 0;

}

function ziehe(farbe, nummer, anzahl) {
	/*
	 * Sich selbst aufrufende Funktion, um nach jeder Bewegung auf dem Spielfeld
	 * 300ms zu pausieren
	 */

	(function zugSchleife(i) {
		setTimeout(function() {
			var r = naechstesFeld(farbe, nummer);
			if (--i && r == 0)
				zugSchleife(i);
			if (i == 0 || r != 0){
				webSocket.send("bereit");
				
			}
				
		}, 300)
	})(anzahl);

}

/* Nur für Testzwecke */
function alleZiel() {
	for ( var i = 0; i < spielfeld.farben.length; i++) {
		for ( var j = 0; j < 4; j++) {
			setzeZiel(spielfeld.farben[i], j);
		}
	}
}

function gebeWuerfel(farbe) {
	if (spielfeld.wuerfel.besitzer != farbe) {
		setze("#wuerfel", spielfeld.wuerfel[farbe].x,
				spielfeld.wuerfel[farbe].y);
		spielfeld.wuerfel.besitzer = farbe;
	}
}

function wuerfele() {

	if (spielfeld.aktiv == true) {
		webSocket.send("wuerfele()");

	}

}

function zeigeWuerfel(farbe, augen) {
	wuerfelPopup(spielfeld.webfarben[farbe], augen);

}

function wuerfelPopup(farbe, augen) {
	$("#popup").css("background-color", farbe);

	document.getElementById("popup").innerHTML = "<p style=\"text-align: center;\"><h2>Es wird gewürfelt...</h2></p>"
			+ "<p style=\"text-align:justify;\">"
			+ "<div style=\"text-align:center;\"><img src=\"/assets/images/wuerfel.gif\"></div> </p>";

	zeigePopup();
	setTimeout(
			function() {
				document.getElementById("popup").innerHTML = "<p style=\"text-align: center;\"><h2>Es wurde eine</h2></p>"
						+ "<p style=\"text-align:justify;\">"
						+ "<div style=\"text-align:center;\"><h1>"
						+ augen
						+ "</h1></div>gewürfelt. </p>";
				setTimeout(function() {
					versteckePopup();
					webSocket.send("bereit");
				}, 1000);
			}, 2000);

}

function winnerPopup() {
	$("#popup").css("background-color", spielfeld.webfarben[spielfeld.farbe]);
	$("#popup").css("width", "600px");
	$("#popup").css("margin-left","-300px");
	$("#popup").css("height", "455px");
	
	document.getElementById("popup").innerHTML = "<p style=\"text-align: center;\"><h2>Herzlichen Glückwunsch! Du hast gewonnen!</h2></p>"
			+ "<div style=\"text-align:center;\"><img src=\"/assets/images/winner.jpg\"></div>";
	
	zeigePopup();
	setTimeout(function(){
		versteckePopup();
		window.location = "/lobbydb";
	},5000);
}

function loserPopup() {
	$("#popup").css("background-color", spielfeld.webfarben[spielfeld.farbe]);
	$("#popup").css("width", "555px");
	$("#popup").css("margin-left","-273px");
	$("#popup").css("height", "500px");
	
	document.getElementById("popup").innerHTML = "<p style=\"text-align: center;\"><h2>Leider hast du verloren!</h2></p>"
		+ "<div style=\"text-align:center;\"><img src=\"/assets/images/loser.png\"></div>";
	
	zeigePopup();
	setTimeout(function(){
		versteckePopup();
		window.location = "/lobbydb";
	},5000);
}

function msgbox(text) {
	document.getElementById("msgbox").innerHTML = text;
}

function instantWin() {
	webSocket.send("instantWin");
}

function naechsterSpieler() {

	for ( var i = 0; i < spielfeld.farben.length; i++) {
		if (spielfeld.wuerfel.besitzer == spielfeld.farben[i]) {
			if (i == spielfeld.farben.length - 1) {
				gebeWuerfel(spielfeld.farben[0]);
				break;
			} else {
				gebeWuerfel(spielfeld.farben[i + 1])
				break;
			}
		}
	}
}

function zeigeFiguren() {
	for ( var i = 0; i < spielfeld.spieler.length; i++) {
		zeige(spielfeld.spieler[i]);
	}
}
/* Spielfeld initialisieren */
function init() {
	if (document.readyState == "complete") {
		for ( var i = 0; i < spielfeld.farben.length; i++) {
			for ( var j = 0; j < 4; j++) {
				setzeHome(spielfeld.farben[i], j);
			}
			verstecke(spielfeld.farben[i]);
		}

	}
}
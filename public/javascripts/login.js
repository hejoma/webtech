

function init(){
	$("#registrierenDiv").hide();
	$("#anmeldenDiv").hide();
	if($("#m").text() == ""){
		$("#m").hide();
	}
}

var reg = false;
var anm = false;

function registrieren(){
	$("#registrierenDiv").slideDown("slow");	
	$("#anmButton").removeClass("btn btn-primary btn-lg").addClass("btn btn-default btn-sm");
	$("#regButton").removeClass("btn btn-default btn-sm").addClass("btn btn-primary btn-lg");	
	if(anm == true){
		$("#anmeldenDiv").hide();
	}
	if(reg == true){
		if($("#neuerUserName").val() != "" && $("#neuesPasswort").val() != "" && $("#passwortWdh").val() != ""){
			if($("#neuerUserName").val().length <= 50 && $("#neuesPasswort").val().length <= 50){
				if($("#neuesPasswort").val() == $("#passwortWdh").val()){
					hashPW();
					return true;
				}else{
					$("#m").html("Passwort wird nicht korrekt wiederholt");
					$("#m").show();
					return false;
				}
			}else{
				$("#m").html("Eingabe darf 50 Zeichen nicht überschreiten");
				$("#m").show();
				return false;
			}
			
			
		}else{
			$("#m").html("Füllen Sie alle Felder aus");
			$("#m").show();
			return false;
		}
		
	}
	reg = true; anm = false;
	return false;
}
function anmelden(){
	$("#anmeldenDiv").slideDown("slow");	
	$("#regButton").removeClass("btn btn-primary btn-lg").addClass("btn btn-default btn-sm");
	$("#anmButton").removeClass("btn btn-default btn-sm").addClass("btn btn-primary btn-lg");
	if(reg == true){
		$("#registrierenDiv").hide();
	}
	if(anm == true){
		if($("#userName").val() != "" && $("#passwort").val() != ""){
			if($("#userName").val().length <= 50 && $("#passwort").val().length <= 50){
				hashPW();
				return true;	
			}else{
				$("#m").html("Eingabe darf 50 Zeichen nicht überschreiten");
				$("#m").show();
				return false;
			}					
		}else{
			$("#m").html("Füllen Sie alle Felder aus");
			$("#m").show();
			return false;
		}
	}
	anm = true; reg = false;
	return false;
}
function hashPW(){
	if(anm == true){
		var pw = $("#passwort").val();
		var hash = CryptoJS.SHA256(pw);
		$("#passwort").val(hash);
	}
	if(reg == true){
		var pw = $("#neuesPasswort").val();
		var hash = CryptoJS.SHA256(pw);
		$("#neuesPasswort").val(hash);
	}
}

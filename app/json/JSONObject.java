package json;

import java.util.HashMap;
import java.util.Map.Entry;

public class JSONObject {
	
	private HashMap<String, Object> map = new HashMap<String,Object>();
	
	public void put(String key, Object obj) {
		map.put(key, obj);
	}
	
	public Object get(String key){
		return map.get(key);
	}
	
	@Override
	public String toString() {
		String result="{ ";
		int c = 0;
		for(Entry<String, Object> e: map.entrySet()) {
			if(c > 0)
				result = result + ", ";
			result = result + "\""+e.getKey()+"\": ";
			if(e.getValue().getClass() == String.class)
				result = result+"\""+e.getValue()+"\"";
			else
				result = result+"\""+e.getValue().toString()+"\"";
			c++;
		}
		result = result + " }";
		return result;
	}

}

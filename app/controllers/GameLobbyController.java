package controllers;

import java.util.Map.Entry;
import java.util.SortedMap;

import play.libs.F.Callback;
import play.libs.F.Callback0;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.WebSocket;
import views.html.GameLobby;
import views.html.login;



public class GameLobbyController extends Controller {
	
	
	public static Result gamelobby(String name) {
		if(play.mvc.Controller.session("username")==null){
			System.out.println("User ohne Session versuchte GameLobby zu betreten");
			return redirect("/");
			
		}

		try {
			int groesse = LobbyController.javagames.get(name).getGROESSE();
			String trittBei = play.mvc.Controller.session("username");
			System.out.println(name + "<--->" + trittBei);
			if (!name.equals(trittBei)) {
				LobbyController.javagames.get(name).SpielerAdden(
						LobbyController.javausers.get(play.mvc.Controller.session("username")));

			} else {
				System.out.println("Erzeuger tritt bei");
			}	

			return play.mvc.Results.ok(GameLobby.render(name, groesse));
		} catch (Exception e) {
			//Lobby, die zu keinem javagames Spiel gehört
			e.printStackTrace();
			play.mvc.Controller.session().clear();
			return play.mvc.Results.ok(login.render(""));
			
		}
	}
	
	
	
	public static WebSocket<String> gamelobbychanges() {

		return new WebSocket<String>() {
			
			//Variablen
			String userle = play.mvc.Controller.session("username");
			SortedMap<String,Spieler> players=LobbyController.getJavausers();
			Spieler player = players.get(userle);

			// Called when the Websocket Handshake is done.
			public void onReady(WebSocket.In<String> in,
					final WebSocket.Out<String> out) {

				// For each event received on the socket,
				in.onMessage(new Callback<String>() {
					public void invoke(String input) {
						//Activities
						
					}
				});

				// When the socket is closed.
				in.onClose(new Callback0() {
					public void invoke() {
//TODO Spieler entfernen

						System.out.println(userle+" Disconnected from gamelobbychanges");

					}
				});

				// Initial
			//Activities
				
				System.out.println(userle+" connected to gamelobbychanges");
				player.setSocket(out);
				player.getAktuellesSpiel().print();
				
				// Weiterleitung aufs Spielfeld
				if(player.getAktuellesSpiel().getPlayers()[player.getAktuellesSpiel().getGROESSE()-1]== player)
					player.getAktuellesSpiel().bungaBunga();
				

			}

		};
	}
	

	

}

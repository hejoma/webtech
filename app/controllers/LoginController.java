package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.login;

import java.util.Map;

import play.db.DB;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

public class LoginController extends Controller {

	public static Result render(String message) {
    	return ok(login.render(message));
    }
    public static Result start() {
    	String message ="";
    	return ok(login.render(message));
    }
    
    public static Result check() {
    	final Map<String, String[]> values = request().body().asFormUrlEncoded();
    	String userName = values.get("userName")[0];
    	String passwortEingabeHash = values.get("passwort")[0];
    	try {
			java.sql.Connection con = DB.getConnection();
			con.setTransactionIsolation(con.TRANSACTION_SERIALIZABLE);
			con.setAutoCommit(false);
			PreparedStatement ps = con.prepareStatement("SELECT COUNT(*) AS zahl FROM USER WHERE NAME= ? ;");
			ps.setString(1, userName);
			ResultSet anzahl = ps.executeQuery();			
			anzahl.next();
			int i = anzahl.getInt("zahl");
			ps.close();
			
			if (i == 1) {
				// User Vorhanden!
				PreparedStatement ps2 = con.prepareStatement("SELECT PWHASH AS pw FROM USER WHERE NAME= ? ;");
				ps2.setString(1, userName);
				ResultSet result = ps2.executeQuery();
				result.next();
				String passwortHashDB = result.getString("pw");
				ps2.close();
				if(passwortHashDB.equals(passwortEingabeHash)){
					con.close();
					session("username", userName);
					return redirect("/lobbydb");
				}else{
					System.out.println("falsches passwort");
					con.close();
					return redirect("/login/Falsches Passwort");
				}							
			} else {
				// User nicht vorhanden!
				System.out.println("User nicht vorhanden");
				con.close();
				return redirect("/login/Username falsch");
			}			
		} catch (SQLException e) {
			System.out.println("Es ist ein Fehler aufgetreten");
		}
    	return redirect("/");
    }
    
    public static Result register() {
    	final Map<String, String[]> values = request().body().asFormUrlEncoded();
    	String neuerUserName = values.get("neuerUserName")[0];
    	String neuesPasswortHash = values.get("neuesPasswort")[0];    	
    	try {
			java.sql.Connection con = DB.getConnection();
			con.setTransactionIsolation(con.TRANSACTION_SERIALIZABLE);
			con.setAutoCommit(false);
			PreparedStatement ps = con.prepareStatement("SELECT COUNT(*) AS zahl FROM USER WHERE NAME= ? ;");
			ps.setString(1, neuerUserName);
			ResultSet anzahl = ps.executeQuery();
			anzahl.next();
			int i = anzahl.getInt("zahl");
			ps.close();
			if (i == 0) {
				// Username noch frei!
				PreparedStatement ps2 = con.prepareStatement("INSERT INTO USER ( NAME , PWHASH , SIEGE , SPIELE ) VALUES (?,?,0,0);");
				ps2.setString(1, neuerUserName);
				ps2.setString(2, neuesPasswortHash);
				ps2.executeUpdate();
				con.commit();
				System.out.println("registriesen erfolgreich");
				ps2.close();
				con.close();
			} else {
				// Username belegt!
				System.out.println("Username schon blegt");
				con.close();
				return redirect("/login/Username schon belegt");				
			}		
		} catch (SQLException e) {
			System.out.println("Es ist ein Fehler aufgetreten");
		}
    	session("username", neuerUserName);
    	return redirect("/lobbydb");
    }    
}
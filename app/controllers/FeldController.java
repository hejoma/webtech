package controllers;

import java.util.Observable;
import java.util.Observer;
import java.util.SortedMap;

import json.JSONObject;
import play.libs.F.Callback;
import play.libs.F.Callback0;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.WebSocket;
import spiel.Spiel;
import spiel.Spielfigur;
import views.html.spielfeld;

public class FeldController extends Controller{

	public static Result spielfeld() {
		
		if(play.mvc.Controller.session("username")==null){
			System.out.println("User ohne Session versuchte SpielFeld zu betreten");
			return redirect("/");
			
		}
		
		return ok(spielfeld.render());
	}
	

	public static WebSocket<String> spiel() {

		return new WebSocket<String>() {

			// Variablen
			String userle = play.mvc.Controller.session("username");
			SortedMap<String, Spieler> players = LobbyController.getJavausers();
			Spieler player = players.get(userle);
			GLInstance lobby = player.getAktuellesSpiel();

			// Called when the Websocket Handshake is done.
			public void onReady(WebSocket.In<String> in,
					final WebSocket.Out<String> out) {

				// For each event received on the socket,
				in.onMessage(new Callback<String>() {
					public void invoke(String input) {
						System.out.println("Empfange: " + input);
						
						if(lobby.getWinner() != -1)
							return;
												
						if (input.startsWith("bereit")) {
							player.setBereit(true);

							if (lobby.alleBereit()) {
								System.out.println("Alle sind bereit");
								
								// Spieler hat gewürfelt;Probiert, ihn ziehen zu lassen
								if (lobby.getSpiel().gewuerfelt()
										&& !lobby.getSpiel().gezogen()) {
									if (lobby.getSpiel().zugMoeglich())
										lobby.getSpiel().probiereZug();
									else
										lobby.getSpiel().naechsterSpieler();

								}
								
								// Spieler hat gezogen, der nächste komt dran
								else if (lobby.getSpiel().gewuerfelt()
										&& lobby.getSpiel().gezogen()) {
									lobby.getSpiel().naechsterSpieler();
								}
							}
						}

						if (player.isActive()) {
							System.out.println("aktiv");
							// Wuerfelt
							if (input.startsWith("wuerfele()")
									&& !lobby.getSpiel().gewuerfelt()) {
								lobby.getSpiel().wuerfele();

							}

							// Spieler hat eine Figur zum Ziehen ausgewählt
							else if (input.contains("Figur")
									&& lobby.getSpiel().gewuerfelt()
									&& !lobby.getSpiel().gezogen()) {

								int nummer = Integer.parseInt(input.split(":")[1]);
								Spielfigur[] figuren = lobby
										.getSpiel()
										.getFiguren(
												lobby.getSpiel()
														.getAktiverSpieler());
								figuren[nummer].ziehe(lobby.getSpiel()
										.getWuerfel());

							}
							
							else if(input.startsWith("instantWin")) {
								lobby.getSpiel().instantWin();
							}

						}

					}
				});

				// When the socket is closed.
				in.onClose(new Callback0() {
					public void invoke() {

						System.out.println(userle + " Disconnected from spiel");

					}
				});

				// Initial
				// Activities

				System.out.println(userle + " connected to spiel");
				player.setSocket(out);
				
				lobby.setAnzahlSpieler(lobby.getAnzahlSpieler() + 1);
				if (lobby.getAnzahlSpieler() == lobby.getGROESSE())
					lobby.losGehts();

			}

		};
	}



}
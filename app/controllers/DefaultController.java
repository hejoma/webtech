package controllers;

import play.libs.F.Callback;
import play.libs.F.Callback0;
import play.mvc.Controller;
import play.mvc.WebSocket;

public class DefaultController extends Controller{

	
	
	public static WebSocket<String> defaultx() {

		
		return new WebSocket<String>() {
			
			//Variablen

			// Called when the Websocket Handshake is done.
			public void onReady(WebSocket.In<String> in,
					final WebSocket.Out<String> out) {

				// For each event received on the socket,
				in.onMessage(new Callback<String>() {
					public void invoke(String input) {
						//Activities
					}
				});

				// When the socket is closed.
				in.onClose(new Callback0() {
					public void invoke() {

						System.out.println("Disconnected");

					}
				});

				// Initial
			//Activities

			}

		};
	}
	
	
	
}

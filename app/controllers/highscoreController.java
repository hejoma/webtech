package controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import play.db.DB;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;

public class highscoreController extends Controller {


	public static Result ajaxDaten(int anzahl) {
		String rueck = "<table><tr><th>Position</th><th>Spieler</th><th>Siege</th></tr>";
		int pos = 1;

		try {
			java.sql.Connection con = DB.getConnection();
			con.setTransactionIsolation(con.TRANSACTION_SERIALIZABLE);
			con.setAutoCommit(false);
			Statement stmt = con.createStatement();

			String query = "SELECT *FROM (SELECT NAME, SIEGE	FROM USER	ORDER BY SIEGE DESC) WHERE ROWNUM <="
					+ anzahl;
			ResultSet result = stmt.executeQuery(query);
			while (result.next()) {
				rueck += "<tr><td>" + pos + "</td><td>"
						+ result.getString("NAME") + "</td><td>"
						+ result.getString("SIEGE") + "</td></tr>";
				pos++;
			}

			stmt.close();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		rueck += "</table";
		return ok(rueck);
	}

}

package controllers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.h2.engine.Session;

import com.fasterxml.jackson.databind.JsonNode;

import play.libs.F.Callback;
import play.libs.F.Callback0;
import play.mvc.*;
import play.mvc.WebSocket.*;
import play.db.DB;
import play.mvc.Result;
import scala.collection.Map;
import views.html.*;

public class LobbyController extends Controller {

	public static SortedMap<String, Spieler> javausers = new TreeMap<String, Spieler>();
	public static HashMap<String, GLInstance> javagames = new HashMap<String, GLInstance>();

	public static SortedMap<String, Spieler> getJavausers() {
		return javausers;
	}

	public static void setJavausers(SortedMap<String, Spieler> javausers) {
		LobbyController.javausers = javausers;
	}

	public static HashMap<String, GLInstance> getJavagames() {
		return javagames;
	}

	public static void setJavagames(HashMap<String, GLInstance> javagames) {
		LobbyController.javagames = javagames;
	}

	public static Result lobbywithdb() {
		if(play.mvc.Controller.session("username")==null){
			System.out.println("User ohne Session versuchte Lobby zu betreten");
			return redirect("/");
			
		}
		String name = play.mvc.Controller.session("username");
		int wins = 0;
		int games = 0;
		int quote = 0;
		beendeteSpieleLoeschen();
		try {
			java.sql.Connection con = DB.getConnection();
			con.setTransactionIsolation(con.TRANSACTION_SERIALIZABLE);
			con.setAutoCommit(false);

			// User Vorhanden!
			Statement stmt = con.createStatement();
			String query = "SELECT Siege,Spiele FROM USER WHERE Name='" + name
					+ "';";
			ResultSet result = stmt.executeQuery(query);
			while (result.next()) {
				wins = result.getInt("Siege");
				games = result.getInt("Spiele");
			}
			quote = (int) Math.round((double) wins / (double) games * 100);
			stmt.close();

			con.close();
		} catch (SQLException e) {
			System.out.println("Es ist ein Fehler aufgetreten");
			play.mvc.Controller.session().clear();
			return play.mvc.Results.ok(login.render(""));

		}

		return play.mvc.Results.ok(lobby.render(name, wins, games, quote));
	}

	public static WebSocket<String> newgame() {

		return new WebSocket<String>() {

			String userle = play.mvc.Controller.session("username");

			// Called when the Websocket Handshake is done.
			public void onReady(WebSocket.In<String> in,
					final WebSocket.Out<String> out) {

				// For each event received on the socket,
				in.onMessage(new Callback<String>() {
					public void invoke(String groesse) {

						// Zeigt an, wer Anfrage schickt
						System.out.println("invoke:" + userle);
						javausers.get(userle).setSocket(null);

						// Zeigt alle Spiele vor neuem Spiel an
						System.out
								.println("---------------------------------------\n"
										+ "Aktuelle Spiel vor neu");
						for (String key : javagames.keySet()) {
							System.out.print("Key" + key);
							System.out.println(javagames.get(key));
						}

						// Zeigt am, wer Spiel erzeugt
						System.out
								.println("************************************\n"
										+ "Erzeuge Spiel mit"
										+ javausers.get(userle).getName()
										+ "\n************************************");

						// Erzeugung neuer LobbyInstanz

						javagames.put(
								userle,
								createGame(javausers.get(userle),
										Integer.parseInt(groesse)));
						// Zeigt alle Spiele nach neuem Spiel an
						System.out.println("Aktuelle Spiel nach neu");
						for (String key : javagames.keySet()) {
							System.out.print("Key: " + key + " GameLobby: ");
							System.out.println(javagames.get(key));
						}

						System.out
								.println("---------------------------------------\n");

						// alle aktualisieren
						for (Entry<String, Spieler> client : javausers
								.entrySet()) {
							Spieler spieler = client.getValue();
							SpieleAusgeben(spieler.getSocket());

						}
					}
				});

				// When the socket is closed.
				in.onClose(new Callback0() {
					public void invoke() {
						// Entfernen des Spielers aus Map
						// javausers.remove(userle);
						System.out.println(userle
								+ "Disconnected from newgame()");

					}
				});

				// Initial
				// Spieler Anlegen
				
				System.out.println(userle + " connected to newgame()");
				Spieler neuerSpieler = new Spieler(userle);
				// Socket zuweisen
				neuerSpieler.setSocket(out);
				// Spieler in Map putten
				javausers.put(neuerSpieler.getName(), neuerSpieler);
				// Ausgabe
				SpielerAusgeben();
				SpieleAusgeben(out);

			}

		};
	}

	public static GLInstance createGame(Spieler spieler, int Groesse) {
		GLInstance back = new GLInstance(spieler, Groesse);
		return back;
	}

	public static Result logout() {
		play.mvc.Controller.session().clear();
		return play.mvc.Results.ok(login.render(""));

	}

	public static void SpieleAusgeben(Out client) {
		String ausgabe = "";
		for (Entry<String, GLInstance> x : javagames.entrySet()) {
			String gewuenschtesSpiel = x.getKey();
			int zaehler = x.getValue().getAnzahlSpieler();
			int nenner = x.getValue().getGROESSE();
			ausgabe = ausgabe
					+ "<p>"
					+ gewuenschtesSpiel
					+ "   -------------   "
					+ zaehler
					+ "/"
					+ nenner
					+ " Spieler   <a href=\"/gamelobby/"
					+ gewuenschtesSpiel
					+ "\"><button onclick=\"closeSocket()\">Beitreten</button></p></a>";
		}

		client.write(ausgabe);

	}

	public static void SpielerAusgeben() {
		System.out.println("Aktuell verbundene Spieler");
		System.out.println("--------------------------");
		for (Entry<String, Spieler> client : javausers.entrySet()) {
			Spieler spieler = client.getValue();
			System.out.println("Spieler:" + spieler.getName());

		}

	}

	public static void beendeteSpieleLoeschen() {
		for (Entry<String, GLInstance> e : javagames.entrySet()) {
			GLInstance instance = (GLInstance) e.getValue();
			if (e.getValue().getWinner() != -1) {
				System.out.println(javagames.toString());
				javagames.remove(e.getKey());
				System.out.println(javagames.toString());
				siegadden(instance.getPlayers()[instance.getWinner()]);
				Spieler[] spieler = instance.getPlayers();
				for (int i = 0; i < spieler.length; i++) {
					if (i != instance.getWinner()) {
						niederlageadden(instance.getPlayers()[i]);
					}
				}
				
				
			}
		}
	}

	public static void siegadden(Spieler spieler) {

		try {
			java.sql.Connection con = DB.getConnection();
			con.setTransactionIsolation(con.TRANSACTION_SERIALIZABLE);
			con.setAutoCommit(false);
			Statement stmt = con.createStatement();
			String update = "UPDATE USER SET SIEGE=SIEGE+1 WHERE NAME='"+spieler.getName()+"';";
			System.out.println(">>ADDE SIEGSIEG >>"+update);
			stmt.executeUpdate(update);
			Statement stmt2 = con.createStatement();
			String update2 = "UPDATE USER SET SPIELE=SPIELE+1 WHERE NAME='"+spieler.getName()+"';";
			
			stmt2.executeUpdate(update2);
			System.out.println(">>ADDE SIEGSPIEL >>"+update2);
			
			stmt.close();
			con.commit();
			con.close();
		} catch (SQLException e) {
			System.out.println("Es ist ein Fehler beim SiegUpdate aufgetreten");
			e.printStackTrace();
		}

	}

	public static void niederlageadden(Spieler spieler) {

		try {
			java.sql.Connection con = DB.getConnection();
			con.setTransactionIsolation(con.TRANSACTION_SERIALIZABLE);
			con.setAutoCommit(false);
			Statement stmt = con.createStatement();
			String update = "UPDATE USER SET SPIELE=SPIELE+1 WHERE NAME='"+spieler.getName()+"';";
			System.out.println(">>ADDE NIEDERLAGESPIEL >>"+update);
			stmt.executeUpdate(update);
			stmt.close();
			con.commit();
			con.close();
		} catch (SQLException e) {
			System.out.println("Es ist ein Fehler beim Niederlagenupdate aufgetreten");
			e.printStackTrace();
		}

	}
	
}

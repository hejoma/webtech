package controllers;

import java.util.Observable;
import java.util.Observer;

import spiel.Spiel;
import json.*;

public class GLInstance implements Observer {
	private int winner;
	private Spieler ERSTELLER;
	private int GROESSE;
	private Spieler[] players;
	private int anzahlSpieler;
	private boolean running;
	private Spiel spiel;

	public GLInstance(Spieler ersteller, int groesse) {
		this.ERSTELLER = ersteller;
		this.GROESSE = groesse;
		players = new Spieler[groesse];
		players[0] = ERSTELLER;
		ERSTELLER.setAktuellesSpiel(this);
		anzahlSpieler = 1;
		running = false;
		winner = -1;

	}

	public int getWinner() {
		return winner;
	}

	public void setWinner(int winner) {
		this.winner = winner;
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public void SpielerAdden(Spieler neuer) throws java.lang.Exception {
		System.out.println("Hallo");
		if (anzahlSpieler == GROESSE) {
			throw new java.lang.Exception("Spiel bereits voll");
		} else {
			players[anzahlSpieler] = neuer;
			neuer.setAktuellesSpiel(this);
			anzahlSpieler++;
			if (anzahlSpieler == GROESSE) {
				running = true;
			}

		}

	}

	// TODO
	public void SpielerEntfernen(Spieler raus) {

	}

	public Spieler getERSTELLER() {
		return ERSTELLER;
	}

	public int getGROESSE() {
		return GROESSE;
	}

	public Spieler[] getPlayers() {
		return players;
	}

	public int getAnzahlSpieler() {
		return anzahlSpieler;
	}

	public void setAnzahlSpieler(int anzahlSpieler) {
		if (this.anzahlSpieler == GROESSE)
			throw new IllegalArgumentException("Spiel ist schon voll");
		this.anzahlSpieler = anzahlSpieler;
	}

	public Spiel getSpiel() {
		return spiel;
	}

	public int getSpielerIndex(Spieler spieler) throws Exception {
		for (int i = 0; i < players.length; i++) {
			if (players[i] == spieler)
				return i;
		}
		throw new Exception("Spieler nicht im Spiel vorhanden!");

	}

	@Override
	public String toString() {
		String back = "GLInstance:" + ERSTELLER.getName() + "_" + GROESSE + "_";
		for (int i = 0; i < players.length; i++) {
			if (players[i] != null) {
				back = back + "P" + (i + 1) + ":" + players[i].getName() + " ";
			}
		}
		return back;
	}

	// Die Berlusconi-Methode; leitet alle Spieler aufs Spielfeld weiter
	public void bungaBunga() {
		JSONObject obj = new JSONObject();
		obj.put("command", "execute");
		obj.put("wert", "function(){bungaBunga();}");
		broadcast(obj.toString());
		anzahlSpieler = 0;
	}

	public boolean alleBereit() {
		boolean result = false;
		int c = 0;
		for (int i = 0; i < players.length; i++) {
			if (players[i].isBereit())
				c++;
		}

		if (c == players.length)
			result = true;

		return result;

	}

	public void setAlleUnBereit() {
		for (int i = 0; i < players.length; i++) {
			players[i].setBereit(false);
		}
	}

	public void bereit() {
		JSONObject obj = new JSONObject();
		obj.put("command", "execute");
		obj.put("wert", "function(){spielfeld.ready = true}");
		broadcast(obj.toString());
	}

	// Nachricht über WebSocket an alle Spieler
	public void broadcast(String msg) {
		for (int i = 0; i < anzahlSpieler; i++) {
			players[i].getSocket().write(msg);
		}

	}

	public void losGehts() {
		spiel = new Spiel(ERSTELLER.getName(), GROESSE, this, getNamenArray());
		spiel.start();

	}

	public void print() {

		String ausgabe = "";
		for (int i = 0; i < anzahlSpieler; i++) {
			if (players[i] != null) {
				ausgabe = ausgabe + "<p>Spieler " + (i + 1) + ": "
						+ players[i].getName() + "</p>";
			}
		}
		JSONObject obj = new JSONObject();
		obj.put("command", "insert");
		obj.put("wert", ausgabe);
		System.out.println(obj.toString());
		broadcast(obj.toString());

	}

	// Update-Methode registriert Änderungen am Spielfeld und aktualisiert die
	// Darstellung für die Nutzer dementsprechend
	@Override
	public void update(Observable arg0, Object arg1) {
		System.out.println("update() wurde aufgerufen");
		String arg = (String) arg1;
		int aktiverSpieler = spiel.getAktiverSpieler();
		System.out.println(arg);
		JSONObject obj = new JSONObject();
		obj.put("command", "execute");

		// Spielfeld ist bereit
		if (arg == "bereit") {
			for (int i = 0; i < anzahlSpieler; i++) {
				obj.put("wert", "function(){spielfeld.farbe = \'"
						+ Spiel.farben[spiel.getReihenfolge()[i]] + "\';}");
				players[i].getSocket().write(obj.toString());
			}

			obj.put("wert", "function(){spielfeld.spieler = "
					+ spiel.getReihenfolgeString().toLowerCase()
					+ "; zeigeFiguren(); }");
			broadcast(obj.toString());
		}

		// Spielr wurde aktiviert
		else if (arg == "aktiviereSpieler()") {
			obj.put("wert", "function(){gebeWuerfel(\'"
					+ Spiel.farben[aktiverSpieler] + "\');}");
			broadcast(obj.toString());
			obj.put("wert", "function(){spielfeld.aktiv = true;}");
			players[spiel.getAktiverSpielerIndex()].getSocket().write(
					obj.toString());
		}

		// Spieler wurde deaktiviert
		else if (arg == "deaktiviereSpieler()") {
			obj.put("wert", "function(){spielfeld.aktiv = false;}");
			players[spiel.getAktiverSpielerIndex()].getSocket().write(
					obj.toString());
		}

		// Spieler hat gewürfelt
		else if (arg == "gewuerfelt") {
			setAlleUnBereit();
			obj.put("wert",
					"function(){zeigeWuerfel(\'"
							+ Spiel.farben[spiel.getReihenfolge()[spiel
									.getAktiverSpielerIndex()]] + "\',"
							+ spiel.getWuerfel() + ");}");
			broadcast(obj.toString());
		}

		// Figur wurde auf Startfeld gestellt
		else if (arg.startsWith("setzeStart(") || arg.startsWith("setzeZiel(")
				|| arg.startsWith("setzeHome(")) {
			obj.put("wert", "function(){" + arg + ";}");
			broadcast(obj.toString());
		}

		// Spieler hat gezogen
		else if (arg.startsWith("ziehe(")) {
			setAlleUnBereit();
			obj.put("wert", "function(){" + arg + ";}");
			broadcast(obj.toString());
		}

		// Spiel wurde gewonnen
		else if (arg.startsWith("Gewinner:")) {
			winner = Integer.parseInt(arg.split(":")[1]);
			obj.put("wert", "function(){winnerPopup();}");
			players[winner].getSocket().write(obj.toString());
			obj.put("wert", "function(){loserPopup();}");
			for (int i = 0; i < anzahlSpieler; i++) {
				if (i != winner)
					players[i].getSocket().write(obj.toString());
			}
		}

		else if (arg.startsWith("msgAktiv:")) {
			String msg = arg.split(":")[1];
			msgAktiv(msg);
		}

		else if (arg.startsWith("msgInaktiv:")) {
			String msg = arg.split(":")[1];
			msgInaktiv(msg);
		}

	}

	// Message für den aktiven Spieler
	public void msgAktiv(String msg) {
		JSONObject obj = new JSONObject();
		obj.put("command", "execute");
		obj.put("wert", "function(){msgbox(\'" + msg + "\');}");
		players[spiel.getAktiverSpielerIndex()].getSocket().write(
				obj.toString());
	}

	// Message für alle inaktiven Spieler
	public void msgInaktiv(String msg) {
		JSONObject obj = new JSONObject();
		obj.put("command", "execute");
		obj.put("wert", "function(){msgbox(\'" + msg + "\');}");

		for (int i = 0; i < players.length; i++) {
			if (i != spiel.getAktiverSpielerIndex())
				players[i].getSocket().write(obj.toString());
		}
	}
	
	private String[] getNamenArray() {
		String[] array = new String[anzahlSpieler];
		for(int i = 0; i< players.length; i++) {
			array[i] = players[i].getName();
		}
		return array;
	}

}

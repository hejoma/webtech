package controllers;




import java.util.Observer;

import play.mvc.Controller;
import play.mvc.WebSocket;
import play.mvc.WebSocket.Out;
import scala.collection.immutable.HashMap;
import scala.util.parsing.json.JSONObject;

public class Spieler {

	private String name;
	private Out socket;
	private Observer controller;
	private GLInstance aktuellesSpiel;
	private boolean bereit = false;

	public GLInstance getAktuellesSpiel() {
		return aktuellesSpiel;
	}

	public void setAktuellesSpiel(GLInstance aktuellesSpiel) {
		this.aktuellesSpiel = aktuellesSpiel;
	}

	public Spieler(String name) {
		super();
		this.name = name;

	}

	public String getName() {
		return name;
	}

	public Out getSocket() {
		return socket;
	}

	public void setSocket(Out socket) {
		this.socket = socket;
	}
	
	
	public Observer getController() {
		return controller;
	}

	public void setController(Observer controller) {
		this.controller = controller;
	}

	public boolean isActive() {
		try {
			if(aktuellesSpiel.getSpielerIndex(this) == aktuellesSpiel.getSpiel().getAktiverSpielerIndex()) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public boolean isBereit() {
		return bereit;
	}

	public void setBereit(boolean bereit) {
		this.bereit = bereit;
	}

	


}

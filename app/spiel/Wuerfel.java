package spiel;

public class Wuerfel {
	
	private int besitzer;
	private int wert;
	
	
	public Wuerfel() {
		super();
		this.besitzer = 0;
		this.wert = 0;
	}
	
	public void wuerfele(){
		double zufall = Math.random()*6+1;
		wert = (int) zufall;
	}

	public int getBesitzer() {
		return besitzer;
	}

	public void setBesitzer(int besitzer) {
		this.besitzer = besitzer;
	}

	public int getWert() {
		return wert;
	}
	
	
	
	

}

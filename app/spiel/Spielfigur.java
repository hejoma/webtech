package spiel;

import json.JSONObject;

public class Spielfigur {

	private int farbe;
	private int nummer;
	private Spielfeld feld;
	private Spielfeld home;
	private Spielfeld ziel;
	private Spiel spiel;

	public Spielfigur(int farbe, int nummer, Spiel spiel) {
		super();
		this.farbe = farbe;
		this.nummer = nummer;
		this.spiel = spiel;
		this.home = new Spielfeld((40 + farbe * 4 + nummer));
		spiel.setHome(farbe, nummer, this.home);
		this.ziel = new Spielfeld((56 + farbe * 4 + nummer));
		spiel.setFinish(farbe, nummer, this.ziel);
		this.setFeld(home);

	}

	public Spielfeld getFeld() {
		return feld;
	}

	public void setFeld(Spielfeld feld) {
		// pruefen, ob Zielfeld leer ist
		
		if (feld.getFigur() != null) {
			Spielfigur zielfigur = feld.getFigur();
			// ist die Figur vom Gegner?
			if (zielfigur.getFarbe() != this.farbe) {
				zielfigur.aufHome();
				System.out.println("Figur "
						+ Spiel.farben[zielfigur.getFarbe()] + " "
						+ zielfigur.getNummer()
						+ " wurde in den Home-Bereich zurückversetzt.");
			}
			// oder ist die Figur vom ausführenden Spieler?
			else {
				System.out
						.println("Dieser Zug kann nicht ausgeführt werden, da das Zielfeld besetzt ist!");
				return;
			}
		}
		if (this.feld != null)
			this.feld.setFigur(null);

		this.feld = feld;
		feld.setFigur(this);
	}


	public Spielfeld getZielfeld(int zuege) {
		int neuesFeld = feld.getId();

		for (int i = 0; i < zuege; i++) {
			neuesFeld = (neuesFeld + 1) % 40;
			
			// Landet der Spieler mit seinem Zug im Zielbereich?
			if (spiel.getFeld(neuesFeld) == spiel.getStartfeld(farbe)
					&& zuege - i > 0) {
				return spiel.getFinish(farbe, nummer);
			}
		}

		Spielfeld zielfeld = spiel.getFeld(neuesFeld);
		
		// Ist das Zielfeld leer?
		if (zielfeld.getFigur() != null) {
			Spielfigur zielfigur = zielfeld.getFigur();
			// ist die Figur vom Gegner?
			if (zielfigur.getFarbe() == this.farbe) {
				zielfeld = feld;
			}
		}

		return zielfeld;

	}

	public void ziehe(int c) {
		
		// Augenzahl prüfen
		if (c < 1 || c > 6)
			throw new IllegalArgumentException(
					"Ungültige Augenzahl eingegeben!");
		
		if(!spiel.running())
			return;

		if (!spiel.figurAufSpielfeld(farbe, nummer))
			return;

		spiel.setGezogen(true);
		Spielfeld neuesFeld = getZielfeld(c);
		
		// Kann dieser Zug ausgeführt werden?
		if (feld == neuesFeld) {
			spiel.setGezogen(false);
			spiel.setChanged();
			spiel.notifyObservers("msgAktiv:Dieser Zug ist leider nicht m&ouml;glich. W&auml;hle eine andere Figur aus.");
			return;
		}

		// Ggf. Figur des Gegners vom Feld schmeißen
		if (neuesFeld.getFigur() != null) {
			neuesFeld.getFigur().aufHome();
		}

		setFeld(neuesFeld);
		spiel.setChanged();
		spiel.notifyObservers("msgInaktiv:"+spiel.getSpielerNamen()[spiel.getAktiverSpielerIndex()]+" zieht "+ c + " Felder.");
		spiel.setChanged();
		spiel.notifyObservers("ziehe(\'" + Spiel.farben[farbe] + "\',"
				+ nummer + "," + c + ")");

	}

	public int getFarbe() {
		return farbe;
	}

	public int getNummer() {
		return nummer;
	}

	public Spielfeld getHome() {
		return home;
	}

	public Spielfeld getZiel() {
		return ziel;
	}

	public Spiel getSpiel() {
		return spiel;
	}

	public void aufStart() {
		if(!spiel.running())
			return;
		setFeld(spiel.getStartfeld(farbe));
		if (feld == spiel.getStartfeld(farbe)) {
			spiel.setChanged();
			spiel.notifyObservers("setzeStart(\'" + Spiel.farben[farbe]
					+ "\'," + nummer + ")");
			spiel.naechsterSpieler();
		}

	}

	public void aufZiel() {
		if(!spiel.running())
			return;
		setFeld(ziel);
		spiel.setChanged();
		spiel.notifyObservers("setzeZiel(\'" + Spiel.farben[farbe] + "\',"
				+ nummer + ")");
		System.out.println("Glückwunsch! " + Spiel.farben[farbe]
				+ " hat jetzt " + spiel.zielCount(farbe) + " im Zielbereich.");
	}

	public void aufHome() {
		if(!spiel.running())
			return;
		setFeld(home);	
		spiel.setChanged();
		spiel.notifyObservers("setzeHome(\'" + Spiel.farben[farbe] + "\',"
				+ nummer + ")");
	}

}

package spiel;

public class Spielfeld {
	
	private int id;
	private Spielfigur figur = null;
	
	
	public Spielfeld(int id) {
		super();
		this.id = id;
		System.out.println(id);
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Spielfigur getFigur() {
		return figur;
	}


	public void setFigur(Spielfigur figur) {
		this.figur = figur;
	}
	
	public String getFeldnummer(){
		String result = "";
		if(id <10)
			result = "f00"+id;
		else
			result = "f0"+id;
		
		return result;
			
	}
	
	
}

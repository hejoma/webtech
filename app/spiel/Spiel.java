package spiel;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

import controllers.GLInstance;
import json.JSONObject;

public class Spiel extends Observable {

	public static final int GELB = 0;
	public static final int BLAU = 1;
	public static final int ROT = 2;
	public static final int GRUEN = 3;

	public static final String[] farben = { "gelb", "blau", "rot", "gruen" };

	private String name;
	private int anzahlSpieler;
	private String[] spielerNamen;

	private Spielfeld[] feld;
	private Spielfeld[][] homes;
	private Spielfeld[][] finish;

	private int[] startfelder = { 0, 20, 10, 30 };
	private int[] endfelder = { 39, 19, 9, 29 };

	private Spielfigur gelb[];
	private Spielfigur rot[];
	private Spielfigur blau[];
	private Spielfigur gruen[];

	private int wuerfel;

	private int[] reihenfolge;
	private int aktiverSpieler;

	private Observer lobby;

	private boolean gewuerfelt;
	private boolean gezogen;

	public Spiel(String name, int anzahlSpieler, Observer lobby, String[] spielerNamen) {
		super();

		// Argumente überprüfen
		if (anzahlSpieler < 2 || anzahlSpieler > 4)
			throw new IllegalArgumentException(
					"Anzahl der Spieler muss zwischen 2 und 4 liegen");
		
		if(spielerNamen.length != anzahlSpieler)
			throw new IllegalArgumentException(
					"Zu wenige Spielernamen!");

		this.name = name;
		this.anzahlSpieler = anzahlSpieler;
		this.feld = new Spielfeld[40];
		this.homes = new Spielfeld[4][4];
		this.finish = new Spielfeld[4][4];
		this.spielerNamen = spielerNamen;
		this.lobby = lobby;
		reihenfolge = new int[anzahlSpieler];

		// Spielfeld erzeugen

		for (int i = 0; i < 40; i++) {
			feld[i] = new Spielfeld(i);
		}

		/*
		 * Spielfiguren incl. Homes und Zielfelder erzeugen und Zugereihenfolge
		 * entsprechend der Spieleranzahl setzen
		 */

		int j;

		gelb = new Spielfigur[4];
		blau = new Spielfigur[4];
		for (j = 0; j < 4; j++) {
			gelb[j] = new Spielfigur(Spiel.GELB, j, this);
		}

		for (j = 0; j < 4; j++) {
			blau[j] = new Spielfigur(Spiel.BLAU, j, this);
		}

		if (anzahlSpieler == 2) {
			reihenfolge[0] = Spiel.GELB;
			reihenfolge[1] = Spiel.BLAU;

		}

		if (anzahlSpieler > 2) {
			rot = new Spielfigur[4];
			for (j = 0; j < 4; j++) {
				rot[j] = new Spielfigur(Spiel.ROT, j, this);
			}
			reihenfolge[0] = Spiel.GELB;
			reihenfolge[1] = Spiel.ROT;
			reihenfolge[2] = Spiel.BLAU;

		}

		if (anzahlSpieler == 4) {
			gruen = new Spielfigur[4];
			for (j = 0; j < 4; j++) {
				gruen[j] = new Spielfigur(Spiel.GRUEN, j, this);
			}

			reihenfolge[3] = Spiel.GRUEN;

		}

		wuerfel = 0;
		aktiverSpieler = 0;
		gewuerfelt = false;
		gezogen = false;
		addObserver(lobby);

	}

	public void start() {
		if (!running())
			return;

		setChanged();
		notifyObservers("bereit");
		aktiviereSpieler();

	}

	public int[] getReihenfolge() {
		return reihenfolge;
	}

	public Spielfigur getFigur(int farbe, int nummer) {
		// Argumente überprüfen
		if (farbe < 0 || farbe > 4 || farbe > (anzahlSpieler - 1) || nummer < 0
				|| nummer > 4)
			throw new IllegalArgumentException(
					"Ungültige Farbe bzw. ungültige Nummer!");

		Spielfigur[] f = getFiguren(farbe);
		return f[nummer];

	}

	public Spielfeld getHome(int farbe, int nummer) {
		return homes[farbe][nummer];
	}

	public void setHome(int farbe, int nummer, Spielfeld home) {
		this.homes[farbe][nummer] = home;
	}

	public Spielfeld getFinish(int farbe, int nummer) {
		return finish[farbe][nummer];
	}

	public void setFinish(int farbe, int nummer, Spielfeld finish) {
		this.finish[farbe][nummer] = finish;
	}

	public Spielfeld getFeld(int id) {
		if (id < 0 || id > 39)
			throw new IllegalArgumentException("Ungültige Feldnummer");
		return feld[id];
	}

	public Spielfeld getStartfeld(int farbe) {
		// Argumente überprüfen
		if (farbe < 0 || farbe > 4 || farbe > (anzahlSpieler - 1))
			throw new IllegalArgumentException("Ungültige Farbe!");

		return feld[startfelder[farbe]];
	}

	public Spielfeld getEndfeld(int farbe) {
		// Argumente überprüfen
		if (farbe < 0 || farbe > 4 || farbe > (anzahlSpieler - 1))
			throw new IllegalArgumentException("Ungültige Farbe!");

		return feld[endfelder[farbe]];
	}
	
	

	public String[] getSpielerNamen() {
		return spielerNamen;
	}

	public void wuerfele() {
		if (!running())
			return;

		if (!gewuerfelt) {
			gewuerfelt = true;
			double zufall = 0;
			
			// Wenn man noch alle 4 Figuren im Home-Bereich hat, erhält man eine doppelte Chance auf eine 6
			if (homeCount(aktiverSpieler) == 4) 
				zufall = Math.random() * 3 + 4;
			else
				zufall = Math.random() * 6 + 1;
			
			wuerfel = (int) zufall;
			gewuerfelt = true;
			setChanged();
			notifyObservers("gewuerfelt");

		}

	}

	public boolean gewuerfelt() {
		return gewuerfelt;
	}

	public boolean gezogen() {
		return gezogen;
	}

	public void setGezogen(boolean gezogen) {
		if (!running())
			return;
		this.gezogen = gezogen;
	}

	public int getWuerfel() {
		return wuerfel;
	}



	public int getAktiverSpieler() {
		return aktiverSpieler;
	}

	public int getAktiverSpielerIndex() {
		int result = 0;
		for (int i = 0; i < anzahlSpieler; i++) {
			if (reihenfolge[i] == aktiverSpieler)
				result = i;
		}
		return result;
	}

	public void setAktiverSpieler(int aktiverSpieler) {
		if (!running())
			return;
		this.aktiverSpieler = aktiverSpieler;
		aktiviereSpieler();

	}

	// Gibt dem nächsten Spieler den Würfel
	public void naechsterSpieler() {
		deaktiviereSpieler();
		if (!running()) {
			System.out.println("Es gibt einen Gewinner");
			setChanged();
			notifyObservers("Gewinner:" + getAktiverSpielerIndex());
			return;

		}

		if (!(gewuerfelt && wuerfel == 6)) {
			for (int i = 0; i < reihenfolge.length; i++) {
				if (reihenfolge[i] == aktiverSpieler) {
					setAktiverSpieler(reihenfolge[(i + 1) % reihenfolge.length]);
					break;
				}
			}
		}
		aktiviereSpieler();

	}

	public String getName() {
		return name;
	}

	public void aktiviereSpieler() {
		if (!running())
			return;
		gewuerfelt = false;
		gezogen = false;
		setChanged();
		notifyObservers("aktiviereSpieler()");
		setChanged();
		notifyObservers("msgAktiv:Du bist am Zug. Klicke auf den Würfel, um zu würfeln.");
		setChanged();
		notifyObservers("msgInaktiv:"
				+ spielerNamen[getAktiverSpielerIndex()]
				+ " ist am Zug. Du musst warten, bis er gewürfelt hat.");
	}

	public void deaktiviereSpieler() {
		setChanged();
		notifyObservers("deaktiviereSpieler()");
	}

	public int homeCount(int farbe) {
		// Argumente überprüfen
		if (farbe < 0 || farbe > 4 || farbe > (anzahlSpieler - 1))
			throw new IllegalArgumentException("Ungültige Farbe!");

		int k = 0;
		for (int i = 0; i < 4; i++) {
			if (homes[farbe][i].getFigur() != null)
				k++;
		}
		return k;
	}

	public int zielCount(int farbe) {
		// Argumente überprüfen
		if (farbe < 0 || farbe > 4 || farbe > (anzahlSpieler - 1))
			throw new IllegalArgumentException("Ungültige Farbe!");

		int k = 0;
		for (int i = 0; i < 4; i++) {
			if (finish[farbe][i].getFigur() != null)
				k++;
		}
		return k;
	}

	// Gibt die nächstbeste Spielfigur im Home-Bereich eines Spielers zurück
	public Spielfigur nextHome(int farbe) {
		// Argumente überprüfen
		if (farbe < 0 || farbe > 4 || farbe > (anzahlSpieler - 1))
			throw new IllegalArgumentException("Ungültige Farbe!");

		Spielfigur result = null;
		for (int i = 0; i < 4; i++) {
			if (homes[farbe][i].getFigur() != null) {
				result = homes[farbe][i].getFigur();
				break;
			}

		}

		return result;

	}

	public Spielfigur[] getFiguren(int farbe) {
		// Argumente überprüfen
		if (farbe < 0 || farbe > 4 || farbe > (anzahlSpieler - 1))
			throw new IllegalArgumentException("Ungültige Farbe!");

		switch (farbe) {
		case Spiel.GELB:
			return gelb;
		case Spiel.BLAU:
			return blau;
		case Spiel.ROT:
			return rot;
		case Spiel.GRUEN:
			return gruen;

		}
		return null;

	}

	public boolean figurAufSpielfeld(int farbe, int nummer) {
		// Argumente überprüfen
		if (farbe < 0 || farbe > 4 || farbe > (anzahlSpieler - 1) || nummer < 0
				|| nummer > 4)
			throw new IllegalArgumentException(
					"Ungültige Farbe bzw. ungültige Nummer!");
		boolean result = false;

		Spielfigur[] f = getFiguren(farbe);

		if (f[nummer].getFeld() != f[nummer].getHome()
				&& f[nummer].getFeld() != f[nummer].getZiel())
			result = true;

		return result;
	}

	// Gibt alle Figuren eines Spielers, die sich auf dem Spielfeld befinden als Collection zurück
	public Collection<Spielfigur> getFigurenAufSpielfeld(int farbe) {
		// Argumente überprüfen
		if (farbe < 0 || farbe > 4 || farbe > (anzahlSpieler - 1))
			throw new IllegalArgumentException("Ungültige Farbe!");

		Collection<Spielfigur> result = new HashSet<Spielfigur>();
		Spielfigur[] f = getFiguren(farbe);

		for (int i = 0; i < f.length; i++) {
			if (figurAufSpielfeld(farbe, i))
				result.add(f[i]);
		}

		return result;

	}

	public String getReihenfolgeString() {
		String order = "[";
		for (int j = 0; j < anzahlSpieler; j++) {
			order += "\'" + Spiel.farben[reihenfolge[j]] + "\'";
			if (j < (anzahlSpieler - 1))
				order += ",";
		}
		order += "]";
		System.out.println(order);
		return order;
	}

	// Überprüft, ob das Spiel noch läuft, d.h. ob es noch keinen Gewinner gibt
	public boolean running() {
		boolean result = true;
		for (int i = 0; i < anzahlSpieler; i++) {
			if (zielCount(i) == 4) {
				result = false;
				break;
			}

		}
		return result;

	}

	// Probiert einen Zwangzug

	public void probiereZug() {

		if (!running())
			return;

		// Situation: Es sind noch alle 4 Figuren im Home-Bereich
		if (homeCount(aktiverSpieler) == 4) {

			if (wuerfel == 6) {
				nextHome(aktiverSpieler).aufStart();
			} else {
				naechsterSpieler();
			}

		}

		// Situation: Es sind noch Figuren im Home-Bereich
		else if (homeCount(aktiverSpieler) > 0) {
			if (wuerfel == 6) {
				nextHome(aktiverSpieler).aufStart();
				if(!gezogen && gewuerfelt)
					probiereStartZug();
			}

			else {
				probiereStartZug();
			}

		}

		// Alle Figuren sind auf dem Spielfeld (oder zum Teil auch schon im
		// Zielbereich)
		else {
			probiereStartZug();
		}

		if (!gezogen && gewuerfelt) {
			setChanged();
			notifyObservers("msgAktiv:Du hast eine "
					+ wuerfel
					+ " gew&uuml;rfelt. Klicke auf eine Spielfigur, um zu ziehen.");
			setChanged();
			notifyObservers("msgInaktiv:"
					+ spielerNamen[getAktiverSpielerIndex()]
					+ " hat eine "
					+ wuerfel
					+ " gew&uuml;rfelt. Du musst warten, bis du wieder dran bist.");
		}

	}

	// Probiert, das Startfeld des aktiven Spielers zu räumen
	public void probiereStartZug() {
		if (!running())
			return;
		if (getStartfeld(aktiverSpieler).getFigur() != null) {
			Spielfigur startfigur = getStartfeld(aktiverSpieler).getFigur();
			if (startfigur.getFarbe() == aktiverSpieler) {
				setChanged();
				notifyObservers("msgAktiv: Du hast noch eine Figur auf dem Startfeld stehen. Du musst das Startfeld räumen.");
				getStartfeld(aktiverSpieler).getFigur().ziehe(wuerfel);

			}
		}
	}

	public boolean zugMoeglich() {

		if (!gewuerfelt || !running())
			return false;

		boolean result = false;
		Collection<Spielfigur> figuren = getFigurenAufSpielfeld(aktiverSpieler);

		// Hat der aktive Spieler überhaupt Figuren auf dem Feld?
		if (figuren.size() > 0)
			result = true;

		// Ist es möglich, eine Figur aufs Startfeld zu stellen?
		if (homeCount(aktiverSpieler) > 0 && wuerfel == 6) {
			result = true;
			
		}
		return result;
	}

	public void setChanged() {
		super.setChanged();
	}
	
	public void instantWin() {
		setChanged();
		notifyObservers("Gewinner:" + getAktiverSpielerIndex());
		
	}

}
